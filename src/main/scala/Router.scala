import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives

class Router(crawlService: CrawlService)(implicit val system: ActorSystem) extends Directives {

  import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
  import JsonFormat._

  def routes() =
    pathPrefix("makeRealCrawl") {
      concat(
        pathEnd {
          concat(
            post {
              entity(as[CrawlerRequestBody]) { crb =>
                onSuccess(crawlService.makeRealCrawl(crb.uris)) { performed =>
                  complete((StatusCodes.OK, performed))
                }
              }
            }
          )
        }
      )
    }
}
