import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import org.slf4j.LoggerFactory

import scala.concurrent.Future


object Main extends App {
  import scala.concurrent.ExecutionContext.Implicits.global
  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val logger = LoggerFactory.getLogger("Simple Crawler")

  val crawlService = new CrawlService()
  val router = new Router(crawlService)

  val handler: Future[Http.ServerBinding] =
    Http()
      .newServerAt("0.0.0.0", 8080)
      .bind(router.routes)

  logger.info("Server started at :8080")
}
