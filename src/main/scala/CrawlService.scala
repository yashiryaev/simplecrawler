import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding.Get
import akka.http.scaladsl.unmarshalling.Unmarshal
import org.slf4j.Logger

import scala.concurrent.{ExecutionContext, Future}

class CrawlService(implicit ec: ExecutionContext, system: ActorSystem, logger: Logger) {

  val title = "<title.*>(.*)</title>".r

  def makeRealCrawl(uris: Seq[String]): Future[CrawlerResponseBody] = {
    Future.sequence {
      uris.map { uri =>
        makeRequest(uri)
          .map(CrawlerResult(uri, _))
      }
    }.map(CrawlerResponseBody)
  }

  private def makeRequest(uri: String): Future[Option[String]] = Future {
    logger.info(s"Asking for $uri")
    Http()
      .singleRequest(Get(uri))
      .flatMap {
        logger.info(s"Answer from $uri received")
        Unmarshal(_).to[String]
      }
      .map(findTitle)
  }.flatten

  private def findTitle(str: String): Option[String] = {
    title.findAllIn(str).matchData.nextOption().map(_.group(1))
  }

}