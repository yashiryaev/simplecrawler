import spray.json.DefaultJsonProtocol._

case class CrawlerRequestBody(uris: Seq[String])

case class CrawlerResult(uri: String, title: Option[String])

case class CrawlerResponseBody(res: Seq[CrawlerResult])

object JsonFormat {
  implicit val requestFormat = jsonFormat1(CrawlerRequestBody)
  implicit val resultFormat = jsonFormat2(CrawlerResult)
  implicit val responseFormat = jsonFormat1(CrawlerResponseBody)
}
