# SimpleCrawler

Сервер запускается на 8080

## Примеры

Запросы отправляются на `/makeRealCrawl`.

Если title был найден, он возвращается в ответе, если не был - нет.

### Запрос

```json
{
  "uris": [
    "https://www.google.com",
    "https://vk.com",
    "https://twitter.com",
    "https://etu.ru"
  ]
}
```


### Ответ

```json
{
  "res": [
    {
      "title": "Google",
      "uri": "https://www.google.com"
    },
    {
      "uri": "https://vk.com"
    },
    {
      "uri": "https://twitter.com"
    },
    {
      "title": "СПбГЭТУ «ЛЭТИ»",
      "uri": "https://etu.ru"
    }
  ]
}
```
